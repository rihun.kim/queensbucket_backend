import nodemailer from 'nodemailer';
import sgTransport from 'nodemailer-sendgrid-transport';

const convertTime = (createdAt) => {
  const time = new Date(createdAt);
  const convertedTime =
    "" + time.getFullYear() + "." + (time.getMonth() + 1) + "." + time.getDate() +
    " / " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();

  return convertedTime;
}

export const makeMailContent = (writer, data) => {
  if (writer === "farmer") {
    return "queensbucket grower 의 [ farmer ] 에 새로운 내용이 생성되었습니다." +
      "제출일 : " + convertTime(data.createdAt) + "<br>" +
      "생산자 / 연락처 : " + data.gName + " / " + data.gPhone + "<br>" +
      "지역 / 작형 : " + data.gLocation + "/ " + data.gType + "<br>" +
      "품목 / 품종 :" + data.gCategory + " / " + data.gSpecies + "<br>" +
      "파종 / 수확일 :" + data.gSewDate + " / " + data.gHarvDate + "<br>" +
      "파종방식 / 식재간격 : " + data.gSewWay + " / " + data.gDistance + "<br>" +
      "토양산도 / 염도 : " + data.gPH + "/" + data.gSalt + "<br>";
  } else if (writer === "producer") {
    return "queensbucket grower 의 [ producer ] 에 새로운 내용이 생성되었습니다." +
      "담당자 : " + data.pName + "<br>" +
      "제출일 : " + convertTime(data.createdAt) + "<br>" +
      "생산자 / 연락처 : " + data.gName + " / " + data.gPhone + "<br>" +
      "지역 / 작형 : " + data.gLocation + "/ " + data.gType + "<br>" +
      "품목 / 품종 :" + data.gCategory + " / " + data.gSpecies + "<br>" +
      "파종 / 수확일 :" + data.gSewDate + " / " + data.gHarvDate + "<br>" +
      "파종방식 / 식재간격 : " + data.gSewWay + " / " + data.gDistance + "<br>" +
      "토양산도 / 염도 : " + data.gPH + "/" + data.gSalt + "<br>" +
      "코드 : " + data.code + "<br>" +
      "창고온도 / 습도 : " + data.sTemp + " / " + data.sHumid + "<br>" +
      "산가 / 벤조피렌 : " + data.oOxid + " / " + data.oBenzo + "<br>" +
      "제조일자 / 제조공장 : " + data.pDate + " / " + data.pLocation + "<br>" +
      "볶음온도상한 / 착유온도 : " + data.pToastTemp + " / " + data.pPressTemp + "<br>"
  }
}

const sendMail = email => {
  const options = {
    auth: {
      api_user: "rihunkim",
      api_key: "qwer12345"
    }
  };
  const client = nodemailer.createTransport(sgTransport(options));

  return client.sendMail(email);
}

export const sendPushMail = (address, content) => {
  const email = {
    from: "hello@queensbucket.co.kr",
    to: address,
    subject: "queensbucket for farmer 에 새로운 데이터가 추가되었습니다.",
    html: `${content}`
  };

  return sendMail(email);
};