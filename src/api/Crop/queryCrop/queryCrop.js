import { prisma } from "../../../../generated/prisma-client";

export default {
  Query: {
    queryCrops: async () => {
      const crops = await prisma.crops();

      return crops;
    }
  }
}