import { prisma } from "../../../../generated/prisma-client";
import { sendPushMail, makeMailContent } from '../../../components/utils';

export default {
  Mutation: {
    createCrop: async (_, args) => {
      const {
        gName, gPhone, gLocation, gType, gCategory, gSpecies,
        gSewDate, gHarvDate, gSewWay, gDistance, gPH, gSalt
      } = args;

      const crop = await prisma.createCrop({
        gName, gPhone, gLocation, gType, gCategory, gSpecies,
        gSewDate, gHarvDate, gSewWay, gDistance, gPH, gSalt
      });

      sendPushMail("rihun.kim@kse.kaist.ac.kr", makeMailContent('farmer', crop));

      return crop;
    }
  }
}