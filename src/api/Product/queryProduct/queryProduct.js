import { prisma } from "../../../../generated/prisma-client";

export default {
  Query: {
    queryProducts: async () => {
      const products = await prisma.products();

      return products;
    }
  }
}