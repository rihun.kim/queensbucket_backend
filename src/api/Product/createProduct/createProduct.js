import { prisma } from "../../../../generated/prisma-client";
import { makeMailContent, sendPushMail } from '../../../components/utils';

export default {
  Mutation: {
    createProduct: async (_, args) => {
      const {
        cid, code,
        gName, gPhone, gLocation, gType, gCategory, gSpecies,
        gSewDate, gHarvDate, gSewWay, gDistance, gPH, gSalt,
        sTemp, sHumid, oOxid, oBenzo, pDate, pLocation, pName, pToastTemp, pPressTemp
      } = args;

      const product = await prisma.createProduct({
        cid, code,
        gName, gPhone, gLocation, gType, gCategory, gSpecies,
        gSewDate, gHarvDate, gSewWay, gDistance, gPH, gSalt,
        sTemp, sHumid, oOxid, oBenzo, pDate, pLocation, pName, pToastTemp, pPressTemp
      });

      //sendPushMail("office@queensbucket.co.kr", makeMailContent('producer', product));

      return product;
    }
  }
}