import { prisma } from "../../../../generated/prisma-client";

export default {
  Mutation: {
    updateProduct: async (_, args) => {
      const {
        code, sTemp, sHumid, oOxid, oBenzo, pDate, pLocation, pName, pToastTemp, pPressTemp,
      } = args;

      const product = await prisma.$exists.product({ code: code });

      if (product) {
        return prisma.updateProduct({
          data: { sTemp, sHumid, oOxid, oBenzo, pDate, pLocation, pName, pToastTemp, pPressTemp },
          where: { code }
        });
      } else {
        throw Error("updateProduct Error");
      }
    }
  }
}

